//#define MAX_SIZE 50

struct vreme
{
	int pocetno, krajnje;
};

typedef struct vreme vreme_t;

void no_1();

void printaj();

void dodaj();

void ukloni();

void no_2();

int exist(int value);

void dodaj_ukloni_cvor(int value);

void no_3();

void ukloni_granu(int a, int b);

void dodaj_granu(int a, int b);

void dodaj_ukloni_granu(int a, int b);

void ispisi_graf();

void obrisi_graf();

int izlazni_stepen(int value);

void no_5();

void no_4();

int ulazni_stepen(int value);

void dostiznost(int a, int b);

void no_6();

void add_queue(int a, int *Q);

int povezanih();

void no_7();

void add_stack (int value, int * S);

int pop_stack(int *S);

int peak_stack(int * S);

int get_index(int value);

vreme_t * ispisi_vremena();

int ** transp(int ** graf);

int visited_all(int * visited);

int find_not_visited(int *visited);

int find_max_time(vreme_t * vremena, int * visited);

int broj_povezanih(vreme_t * vremena, int ** graf_t);