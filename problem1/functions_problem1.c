#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>

#include "problem1.h"

/* extern variables */

extern int size;
extern int * values;
extern int **graf;
extern int *used;

/* global variables for queue */

int rear = -1, front = -1;

/* global variables for stack */

int stack_pointer = -1;   // points to last used space. empty when stack_pointer == -1

int get_index(int value)
{
	
	int i;

	for ( i = 0; i < size; i++)
		if ( values[i] == value)
			return i;
	return -1;
}

void no_1() /* priprema terena xD */
{
	int i;
	printf("Unesite broj cvorova: ");  // maksimalan
	scanf("%d", &size);

	graf = (int **) calloc(size, sizeof(int*));
	used = (int *) calloc(size, sizeof(int));
	values = (int *) calloc(size, sizeof(int));

	for ( i = 0; i < size; i++)
		graf[i] = (int *) calloc(size, sizeof(int));

	if ( graf)
		printf("Graf je uspesno kreiran\n");

}

void printaj()
{
	printf("\t\t\t\t======================================\n");
	printf("\t\t\t\t|                                    |\n");
	printf("\t\t\t\t| [1] Kreiraj praznu strukturu       |\n");
	printf("\t\t\t\t| [2] Dodaj/Ukloni cvor              |\n");
	printf("\t\t\t\t| [3] Dodaj/Ukloni granu             |\n");
	printf("\t\t\t\t| [4] Izracunaj ulazni stepen cvora  |\n");
	printf("\t\t\t\t| [5] Izracunaj izlazni stepen cvora |\n");
	printf("\t\t\t\t| [6] Odredjivanje dostiznosti       |\n");
	printf("\t\t\t\t| [7] Broj povezanih komponenti      |\n");
	printf("\t\t\t\t| [8] Ispisi graf                    |\n");
	printf("\t\t\t\t| [0] Kraj                           |\n");
	printf("\t\t\t\t|                                    |\n");
	printf("\t\t\t\t======================================\n");
}

void no_2()
{
	int value;
	printf("Unesite sadrzinu [info] cvora: ");
	scanf("%d", &value);
	dodaj_ukloni_cvor(value);
}

int get_avail()
{
	int i;
	for ( i = 0; i < size; i++)
		if (used[i] == 0)
			return i;
	return -1;
}

void dodaj_cvor( int value)
{
	int avail = get_avail();
	used[avail] = 1;
	values[avail] = value;
	printf("Cvor uspesno dodat\n");
}

void ukloni_cvor(int value)
{
	int index = get_index(value), i;
	used[index] = 0;
	
	for ( i = 0; i < size; i++)
	{
		graf[index][i] = 0;
		graf[i][index] = 0;
	}
	printf("Cvor uspesno uklonjen\n");
}

void dodaj_ukloni_cvor(int value)
{
	//int index = get_index(value);
	//int avail = get_avail();
	if ( exist_cvor(value))
		ukloni_cvor(value);
	else
		if ( get_avail() == -1)
			printf("Nema vise mesta\n");
		else
			dodaj_cvor(value);
}

int exist_cvor(int value)
{
	int index = get_index(value);
	if ( index == -1)
		return 0;
	else
		if (used[index] == 0)
			return 0;
	return 1;
}

void no_3()
{
	int a,b;
	printf("Grana kojom operisete je usmerena od cvora A ka dcoru B\n");
	printf("Unesite cvor A: ");
	scanf("%d", &a);
	printf("Unesite cvor B: ");
	scanf("%d", &b);
	dodaj_ukloni_granu(a,b);
}

int exist_grana(int a, int b)
{
	if ( exist_cvor(a) && exist_cvor(b))
		if (graf[get_index(a)][get_index(b)] != 0)
			return 1;
		else
			return 0;
	return 0;
}

void dodaj_ukloni_granu(int a, int b)
{
		if ( exist_grana(a,b))
			ukloni_granu(a,b);
		else 
			if ( exist_cvor(a) && exist_cvor(b))
				dodaj_granu(a,b);
			else 
				printf("Neki od zadatih cvorova ne postoji\n");
}

void ukloni_granu(int a, int b)
{
	graf[get_index(a)][get_index(b)] = 0;
	printf("Grana uspesno uklonjena\n");
}

void dodaj_granu(int a, int b)
{
	graf[get_index(a)][get_index(b)] = 1;
	printf("Grana uspesno dodata\n");
}

void ispisi_graf()
{
	int i,j;
	printf("    ");
	for ( i = 0; i < size; i ++)
		printf("%3d ", used[i]);
	printf("\n");
	printf("    ");

	for ( i = 0; i < size; i++)
		printf("%3d ", values[i]);
	printf("\n\n");

	for ( i = 0; i < size; i++)
	{
		printf("%3d ", values[i]);
		for ( j = 0; j < size; j++)
			printf("%3d ", graf[i][j]);
		printf("\n\n");
	}
}

void obrisi_graf()
{
	int i;
	for ( i = 0; i < size; i++)
		free(graf[i]);
	free(graf);
	free(values);
	free(used);
}

void no_5()
{
	int value;
	printf("Unesite cvor: ");
	scanf("%d", &value);
	if ( exist_cvor(value))
		printf("Izlazni stepen cvora %d je %d\n", value, izlazni_stepen(value));
	else 
		printf("Cvor ne postoji\n");
}

int izlazni_stepen(int value)
{
	int ret = 0;
	int i;
	int index = get_index(value);
	for (i = 0; i < size; i++)
	{
		if (graf[index][i] != 0)
			ret++;
	}
	return ret;
}

void no_4()
{
	int value;
	printf("Unesite cvor: ");
	scanf("%d", &value);
	if ( exist_cvor(value))
		printf("Ulazni stepen cvora %d je %d\n", value, ulazni_stepen(value));
	else 
		printf("Cvor ne postoji\n");
}

int ulazni_stepen(int value)
{
	int ret = 0;
	int i;
	int index = get_index(value);
	for (i = 0; i < size; i++)
	{
		if (graf[i][index] != 0)
			ret++;
	}
	return ret;
}

void dostiznost(int a, int b)
{

	int * S = (int *) malloc(size * sizeof(int));
	int * visited = (int *) calloc(size, sizeof(int));
	int * length = (int *) calloc(size, sizeof(int));
	int found = 0;
	int len = 0;
	int * temp_s;

	length[get_index(a)] = len;
	add_stack(a, S);
	visited[get_index(a)] = 1; 
	while (! stack_empty(S))
	{
		int tekuci = peak_stack(S);

		if ( num_avail(get_index(tekuci), visited) == 0)
		{
			pop_stack(S);
		}
		else
		{
			int i;
			len = length[get_index(tekuci)];
			len++;
			for ( i = 0; i < size; i++) // baci sve neposecene na stek;
				if ( graf[get_index(tekuci)][i] && (! visited[i]) )   // ako grana postoji, i ako nije posecen taj cvor, updatuj mu length, poseti ga, proveri da li je to trazeni cvor, i baci ga na stek
				{
					visited[i] = 1;
					length[i] = len;
					add_stack(values[i], S);
					if ( values[i] == b)
						found = 1;
				}
		}

		if (found)
			break;

	}



	if ( found )
	{
		int pointer = 0, tekuci, i;
		temp_s = (int *) malloc((length[get_index(b)]+ 1) * sizeof(int));  // duzina puta
		
		for( i = 0; i < length[get_index(b)]; i++)
		{
			tekuci = peak_stack(S);
			temp_s[pointer++] = pop_stack(S);
		
			while ( length[get_index(tekuci)] == length[get_index(peak_stack(S))] )
				pop_stack(S);

		}
		temp_s[pointer++] = a;
		while(pointer > 0)
			printf("%d ", temp_s[--pointer]);

		free(temp_s);
	}
	else
		printf("Ne postoji veza\n");
		
	free(S);
	free(length);
}

int num_avail(int index, int * visited) // return
{
	//extern int * visited;
	int ret = 0;
	int i;
	if ( index >= 0)
	for ( i = 0; i < size; i++)
	if ((graf[index][i] != 0) && !visited[i])
		ret++;

	return ret;
}

void no_6()
{
	int a,b;
	printf("Unesite cvor A: ");
	scanf("%d", &a);
	printf("Unesite cvor B: ");
	scanf("%d", &b);
	if ( exist_cvor(a) && exist_cvor(b))
		dostiznost(a,b);
	else
		printf("Neki od cvorova ne postoji\n");

}

void no_7()
{
	int i;
	vreme_t * vremena = ispisi_vremena();
	int ** graf_t = transp(graf);
	printf("Broj povezanih komponenti u grafu je %d\n", broj_povezanih(vremena, graf_t));
	free(vremena);
	for ( i = 0; i < size; i++)
		free(graf_t[i]);
	free(graf_t);
}

/* queue functions */

int queue_empty(int *Q)
{
	return front == -1 ? 1 : 0;
}

void napisi_red(int *Q)
{
	int i;
	//extern int front, rear;
	printf("front je: %d\nrear = %d\n", front, rear);
	for ( i = 0; i < size; i++)
	{
		printf("%d, ", Q[i]);
	}
	printf("\n");
}

void add_queue(int a, int *Q)
{
	//extern int rear, front;
	rear = rear % (size -1);
		rear++;
	if ( front == rear)
		printf("Red je pun\n");
	else
	{
		
		Q[rear] = a;
		if ( front == -1)
			front = 0;
	}
}

int remove_queue(int *Q)
{
	//extern int front, rear;

	if ( front == -1)
		printf("Red je prazan\n");
	else
	{
		int x = Q[front];
		if ( front == rear)
			front = rear = -1;
		else
			front = (front % (size - 1)) + 1;
		return x;
	}

}

/* stack functions */

int stack_empty(int *S)
{
	if (stack_pointer == -1)
		return 1;
	else 
		return 0;
}

void add_stack (int value, int * S)
{
	if ( stack_pointer == size - 1)
		printf("Stack full\n");
	else
		S[++stack_pointer] = value;
}

int pop_stack(int *S)
{
	if (! stack_empty(S))
		return S[stack_pointer--];
	else
		printf("Stack empty\n");
}

int peak_stack(int * S)
{
	if (! stack_empty(S))
		return S[stack_pointer];
	else
		printf("Stack empty\n");
}

int broj_povezanih(vreme_t * vremena, int ** graf_t)
{
	int * S = (int *) calloc(size, sizeof(int));
	int * visited = (int *) calloc(size, sizeof(int));
	int ret = 0;
	int tekuci;

	while ( !visited_all(visited))
	{
		tekuci = values[find_max_time(vremena, visited)];
		add_stack(tekuci, S);
		visited[get_index(tekuci)] = 1;
		ret++;
		while (! stack_empty(S))
		{
			int i;
			int provera = 0;
			for ( i = 0; i < size; i++)
				if (graf_t[get_index(tekuci)][i] && (! visited[i]) && used[i])
				{
					visited[i] = 1;
					add_stack(values[i], S);
					provera++;
				}
			if ( ! provera)
				tekuci = pop_stack(S);
		}
	}

	free(visited);
	free(S);
	return ret;
}

int find_max_time(vreme_t * vremena, int * visited)
{
	int i;
	int max = 0;
	int index;

	for ( i = 0; i < size; i++)
		if ( (vremena[i].krajnje > max) && (! visited[i]))
		{
			max = vremena[i].krajnje;
			index = i;
		}
	
		return index;
}

vreme_t * ispisi_vremena()
{
	vreme_t * vremena = (vreme_t *) calloc(size, sizeof(vreme_t));
	int time = 0;
	int * S = (int *) calloc(size, sizeof(int));
	int *visited = (int *) calloc(size, sizeof(int));
	int tekuci;

	

	while ( ! visited_all(visited))
	{
		tekuci = values[find_not_visited(visited)];
		visited[get_index(tekuci)] = 1;
		vremena[get_index(tekuci)].pocetno = ++time;
		add_stack(tekuci, S);
		while ( ! stack_empty(S))
		{
			int provera = 0;
			int i;

			tekuci = peak_stack(S);

			for ( i = 0; i < size; i++)
				if ( graf[get_index(tekuci)][i] && (! visited[i]) && used[i])
				{
					visited[i] = 1;
					add_stack(values[i], S);
					vremena[i].pocetno = ++time;
					provera++;
				}
			if ( ! provera)
			{
				tekuci = pop_stack(S);
				vremena[get_index(tekuci)].krajnje= ++time;
			}
		}
	}
	free(visited);
	free(S);
	return vremena;
	
}

int ** transp(int ** graf)
{
	int ** graf_t = (int **) calloc(size, sizeof(int *));
	int i, j;

	for ( i = 0; i < size; i++)
		graf_t[i] = (int *) calloc(size, sizeof(int *));

	for ( i = 0; i < size; i++)
		for( j = 0; j < size; j++)
			graf_t[i][j] = graf[j][i];

	return graf_t;
}

int visited_all(int * visited)
{
	int i;

	for ( i = 0; i < size; i++)
		if (! visited[i] && used[i])
			return 0;
	return 1;
}

int find_not_visited(int *visited)
{
	int i;

	for ( i = 0; i < size; i++)
	if ( ! visited[i] && used[i])
		return i;
}

