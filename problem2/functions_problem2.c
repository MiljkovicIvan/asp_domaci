#include <stdlib.h>
#include <stddef.h>
#include "problem2.h"
#include <stdio.h>

int get_difference(char *first, char * second)
{
	int ret = 0;
	int i;
	for ( i = 0; i < 10; i++)
		ret += get_difference_one(first[i], second[i]);
	return ret;
}

int get_difference_one(char a, char b)
{
	if (( a >= '5' && b >= '5') || (a <= '5' && b <= '5'))
		return a-b > 0 ? a-b : b-a;
	else 
		return minimum (a-b > 0 ? a-b : b-a, a >= b ? '9' + 1 - a + b - '0' : '9' + 1 - b + a - '0'); 
}


int minimum(int a, int b)
{
	return a >= b ? b : a;
}

void find_path(int n)
{
	extern unsigned int ** graf;
	int i,j;
	int *visited;
	FILE *izlaz = fopen("izlaz.txt", "w");
	int total_moves = 0;
	visited = (int *) calloc(n+1, sizeof(int));

	for ( i = 0; i <= n; i++)
		graf[i][i] = -1;

	/*		//ispis grafa
	for ( i = 0; i <= n; i++)
	{
		for ( j = 0; j < n; j++)
			printf("%10u ", graf[i][j]);
		printf("\n");
	}
	*/
	visited[n] = 1;  // 0 node

	for ( i = 0; i < n; i++)
	{
		j = find_min(visited, n, &total_moves);
		printf("%d\n", j);
	}

	printf("ukupno je bilo poteza: %d\n", total_moves);
	

	
}


int find_min(int * visited, int n, int * total)
{
	extern unsigned **graf;
	int i, j;
	unsigned int min = -1;
	int index_i, index_j;
	for ( i = 0; i <= n; i++)
		if ( visited[i] )
			for ( j = 0; j < n; j++)
				if ( !visited[j])
					if ( graf[i][j] < min)
					{
						min = graf[i][j];
						index_i = i;
						index_j = j;
					}
	visited[index_j] = 1;
	*total += graf[index_i][index_j];			
	
	return index_j + 1;

}