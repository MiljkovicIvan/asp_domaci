#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include "problem2.h"

unsigned int **graf;
char ** values;


void main()
{
	FILE * ulaz = fopen("test.txt", "r");
	char space;
	int n, i, j;
	time_t start, end;
	fscanf(ulaz, "%d%c", &n, &space);

	values = (char **) calloc(n+1, sizeof(char *));
	graf = (unsigned int **) calloc(n+1, sizeof(int *));

	for ( i = 0; i < n; i++)
	{	
		values[i] = (char *) calloc(10, sizeof(char));
		for ( j = 0; j < 10; j++)
			fscanf(ulaz, "%c", &values[i][j]);
		values[i][j] = '\0';
		fscanf(ulaz, "%c", &space);
		graf[i] = (unsigned int *) calloc(n, sizeof(int));
	}

	graf[n] = (unsigned int *) calloc(n, sizeof(int));
	values[n] = (char *) calloc(10, sizeof(char));
	values[n] = "0000000000";

	//for ( i = 0; i < n; i++)
	//	printf("%s\n", values[i]);
	
	for ( i = 0; i <= n; i++)
		for ( j = 0; j <= n; j++)
		{
			graf[i][j] = get_difference(values[i], values[j]);
		}

	find_path(n);

	system("pause");
}